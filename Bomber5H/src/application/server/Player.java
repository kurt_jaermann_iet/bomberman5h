package application.server;

/**
 * Created by john on 20.11.16.
 */
public class Player {
    String username;
    private String status;
    private int positionX;
    private int positionY;

    public void Player(){
        positionX = 0;
        positionY = 0;
        status = "alive";
    }

    public void Player(int positionX, int positionY)
    {
        this.positionX = positionX;
        this.positionY = positionY;
        status = "alive";
    }

    public void setpositionX(int positionX)
    {
        this.positionX = positionX;
    }

    public void setpositionY(int positionY)
    {
        this.positionY = positionY;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public int getPositionX()
    {
        return positionX;
    }

    public int getPositionY()
    {
        return positionY;
    }

    public String getStatus()
    {
        return status;
    }

    public String getUsername()
    {
        return username;
    }

    public boolean kill(){
        status = "dead";

        boolean success = true;
        return success;
    }

    public boolean move(int posX, int posY){

        positionX = posX;
        positionY = posY;

        boolean success = true;
        return success;
    }

    //Standard 1
    public boolean moveRight(int count)
    {
        //Informieren best Practice Map mitgeben
        int lengthMap = Game.getMap().fields.length();
        if(positionX + count <= lengthMap)
        {
            positionX =+ count;
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean moveLeft(int count)
    {
        if(positionX - count >= 0)
        {
            positionX =- count;
            return true;
        }
        else
        {
            System.out.println("Cannot move any further!");
            return false;
        }
    }

    public boolean moveUp(int count)
    {
        if(positionY - count >= 0)
        {
            positionY =- count;
            return true;
        }
        else
        {
            System.out.println("Cannot move any further!");
            return false;
        }
    }

    public boolean moveDown(int count)
    {
        //Informieren best Practice Map mitgeben
        int lengthMap = Game.getMap().fields.length();
        if(positionY + count <= 0)
        {
            positionY =+ count;
            return true;
        }
        else
        {
            System.out.println("Cannot move any further!");
            return false;
        }
    }
}
