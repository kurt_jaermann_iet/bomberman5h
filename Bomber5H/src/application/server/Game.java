package application.server;

import java.util.List;

/**
 * Created by john on 20.11.16.
 */
public class Game {
    List<Player> players;
    Map map;

    public void Game(){

    }

    public Map getMap()
    {
        return map;
    }

    public boolean startGame(String labyrinth){
        map = new Map();

        boolean success = true;
        return success;
    }

    public void beginGame()
    {

    }

    public boolean playerJoined(){
        players.add(new Player());

        if(players.size() == 4)
        {
            beginGame();
        }

        return true;
    }

    public boolean movePlayer(Player player, int positionX, int positionY){

        player.move(positionX, positionY);

        boolean success = true;
        return success;
    }

    public boolean dropBomb(int positionX, int positionY){
        map.placeBomb(positionX, positionY);

        boolean success = true;
        return success;
    }

    public boolean bombExploded(){

        boolean success = true;
        return success;
    }

    public boolean playerHit(Player player){
        // Kill the player if hit
        player.kill();

        boolean success = true;
        return success;
    }

    public boolean handleMessage(String username, String message, String recipient){
        if (recipient == "") {
            // if no recipients, send to all clients
            String type = "all";
        } else {
            String type = recipient;
        }
        Message mymessage = new Message();


        boolean success = true;
        return success;
    }
}
