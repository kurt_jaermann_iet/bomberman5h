package application.server.api;

import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import network.Message;

public class MessageQueue {
	Queue<Message> messageQueue;
	public synchronized void addMessage(Message message){
		messageQueue.add(message);
	}
	
	public synchronized void removeMessage(Message message){
		messageQueue.remove();
	}
	ExecutorService executor = Executors.newFixedThreadPool(4);
}
