package application.server.api;

import com.sun.xml.internal.ws.resources.AddressingMessages;

import network.Message;
import network.server.ServerApplicationInterface;

public class ServerApplication implements ServerApplicationInterface{

	@Override
	public synchronized void handleMessage(Message message, String connectionId) {
		MessageQueue queue = new MessageQueue();
		queue.addMessage(message);
	}

}
