package application.server;

import java.util.Date;

/**
 * Created by john on 20.11.16.
 */
public class Message {
    Network n = new Network(); // TODO
    String type;
    String message;
    Date timestamp;

    public void message(String type, String message){
        // send message
        if (type =="all"){
            n.sendAllClients(message);
        }
        else {
            n.sendSingleClient(type, message);
        }

    }
}
