package network;

import java.io.Serializable;

/**
 * Alle Klassen für Nachrichten, welche zwischen dem Bomberman-Server und den Bomberman-Clients
 * ausgetauscht werden, müssen dieses Interface implementieren.
 * 
 * @author Andres Scheidegger
 *
 *
 * @author Kurt Järmann
 * Message implements serializable now to makesure every message
 * can be transfered without any trouble
 */
public interface Message extends Serializable{ 
}
